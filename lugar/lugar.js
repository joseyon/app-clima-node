const axios = require('axios');

const getLugarLatLng= async (direccion) =>{

    let encondeUrl = encodeURI(direccion);

    let response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encondeUrl}&key=AIzaSyA-HXVa2jtkGfKtIJwisxgC46RaWqC1xuI`);
        
    if (response.data.status=== 'ZERO_RESULTS'){
        throw new Error(`No hay resultados para la Ciudad ${direccion}`);
        // en el catch del llamado lo que devuelve es el twrow el cual hace un return automatico
    }
    
    let nombre= response.data.results[0].formatted_address;
    let latitud= response.data.results[0].geometry.location.lat;
    let longitud= response.data.results[0].geometry.location.lng;
    
    return{
        direccion: nombre,
        latitud: latitud,
        longitud: longitud
    }

    // cuando llamen a la funcion y sea exitoso en el "then" del llamado lo que se devuelve es el return   
    
}

module.exports = {
    getLugarLatLng
}