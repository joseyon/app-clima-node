const lugar = require ('./lugar/lugar');

const clima = require ('./clima/clima');

const argv = require('yargs').options({
    direccion:{
        alias:'d',
        desc:'Direccion de la Ciudad para obtener el clima',
        demand: true
    }
}).argv;

let getInfo = async (direccion)=>{

    try{
        let coord = await lugar.getLugarLatLng(direccion);
    
        let temp = await clima.getClima(coord.latitud, coord.longitud);

        return `El clima en ${coord.direccion} es de ${temp}`;
    }
    catch(e){

        return (`ERROR: No se pudo determinar el clima en: ${ direccion}`);
    }
}


getInfo(argv.direccion) 
    .then(rsp=>{
        console.log(rsp);
    })

    .catch(e=>{
        console.log(e);
    });